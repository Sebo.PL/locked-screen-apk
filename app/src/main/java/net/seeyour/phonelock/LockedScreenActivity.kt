package net.seeyour.phonelock

import android.app.Activity
import android.os.Bundle
import android.widget.Button
import net.seeyour.phonelock.utils.LockedScreenUtils

/**
 */
class LockedScreenActivity : Activity() {
    private lateinit var lockedScreenUtils: LockedScreenUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lockedscreen)
        lockedScreenUtils = LockedScreenUtils()
        findViewById<Button>(R.id.unlock_button).setOnClickListener {
            finish()
        }
    }

    companion object
}