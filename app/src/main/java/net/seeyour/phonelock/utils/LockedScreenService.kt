package net.seeyour.phonelock.utils

import android.app.NotificationChannel
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.content.BroadcastReceiver
import android.content.IntentFilter
import androidx.core.app.NotificationCompat
import net.seeyour.phonelock.R

class LockedScreenService : Service() {
    private lateinit var broadcastReceiver: BroadcastReceiver

    override fun onBind(
        intent: Intent,
    ): IBinder? = null

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int,
    ): Int {
        broadcastReceiver = LockedScreenIntentReceiver()
        registerReceiver(
            broadcastReceiver,
            IntentFilter(Intent.ACTION_SCREEN_OFF).apply {
                addAction(Intent.ACTION_SCREEN_ON)
            }
        )
        startForeground()
        return START_STICKY
    }

    private fun startForeground() {
        startForeground(
            9999,
            NotificationCompat.Builder(
                this,
                NotificationChannel.DEFAULT_CHANNEL_ID,
            )
                .setContentText("Running")
                .setContentTitle(resources.getString(R.string.app_name))
                .setContentIntent(null)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setTicker(resources.getString(R.string.app_name))
                .build(),
        )
    }
}