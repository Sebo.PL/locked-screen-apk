package net.seeyour.phonelock.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import net.seeyour.phonelock.LockedScreenActivity

class LockedScreenIntentReceiver : BroadcastReceiver() {

    override fun onReceive(
        context: Context,
        intent: Intent,
    ) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        if (
            Intent.ACTION_BOOT_COMPLETED == intent.action ||
            Intent.ACTION_SCREEN_OFF == intent.action ||
            Intent.ACTION_SCREEN_ON == intent.action
        ) {
            lockScreen(context)
        }
    }

    // Lock the screen.
    private fun lockScreen(
        context: Context,
    ) {
        context.startActivity(
            Intent(
                context,
                LockedScreenActivity.javaClass,
            ).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            },
        )
    }
}